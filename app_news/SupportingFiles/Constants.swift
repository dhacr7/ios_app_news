import Foundation

class Constants {
    public static let baseURL = "https://api.themoviedb.org/3/movie"
    public static let searchURL = "https://api.themoviedb.org/3/search/movie"
    public static let API_KEY = "63f0c91d338a102be02e9e2f38c3f9e5"
    static let imageUrl = "https://image.tmdb.org/t/p/w500"
}
