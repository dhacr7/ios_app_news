import Foundation

import Foundation
import Alamofire
import UIKit

class MovieDetailInteractor: MovieDetailPresenterToInteractorProtocol {
    
    weak var presenter: MovieDetailInteractorToPresenterProtocol?
    var movieDetailFetchData: MovieDetailModel?
    
    func fetchMovies(param: Int) {
        let setUrl = Constants.baseURL+"/\(param)"+"?api_key="+Constants.API_KEY+"&language=en-US"
        AF.request(setUrl).response { response in
            if (response.response?.statusCode == 200){
                guard let data = response.data else {return}
                do {
                    let decoder = JSONDecoder()
                    let movieResponse = try decoder.decode(MovieDetailModel.self, from: data)
                    self.movieDetailFetchData = movieResponse
                    self.presenter?.movieDetailFetched()
                } catch let error{
                    print(error)
                }
            }else{
                self.presenter?.movieDetailFetchedFailed()
            }
        }
    }
    
}

