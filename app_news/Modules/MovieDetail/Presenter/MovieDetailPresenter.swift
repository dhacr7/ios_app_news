import Foundation
import UIKit


class MovieDetailPresenter: MovieDetailViewToPresenterProtocol {

    var view: MovieDetailPresenterToViewProtocol?
    var interactor: MovieDetailPresenterToInteractorProtocol?
    var router: MovieDetailPresenterToRouterProtocol?
    var param: Int!
    
    convenience init (_ param: Int) {
        self.init()
        self.param = param
    }

    func updateDetailView() {
        interactor?.fetchMovies(param: param)
    }
    
    func getMovieDetailData() -> MovieDetailModel? {
        return interactor?.movieDetailFetchData
    }
    
}

extension MovieDetailPresenter: MovieDetailInteractorToPresenterProtocol {
    func movieDetailFetched() {
        view?.showMovies()
    }
    
    func movieDetailFetchedFailed() {
        view?.showError()
    }
    
}
