import UIKit

class MovieDetailViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var imageMovieDetail: UIImageView!
    
    // MARK: - Properties
    var presenter: MovieDetailViewToPresenterProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

}

// MARK: - Setup View
extension MovieDetailViewController {
    
    func setup(){
        bindPresenter()
    }
    
    func bindPresenter(){
        guard let presenter = presenter else { return }
        presenter.updateDetailView()
    }
    
    func configure(data: MovieDetailModel){
        titleLabel.text = data.title
        overviewLabel.text = data.overview
        let url = URL(string: Constants.imageUrl+data.poster_path!)
        imageMovieDetail.load(url: url!)
    }
}

// MARK: - MovieListPresenterToViewProtocol
extension MovieDetailViewController: MovieDetailPresenterToViewProtocol {

    func showMovies() {
        let movieDetailData = presenter?.getMovieDetailData()
        configure(data: movieDetailData!)
    }
    
    func showError() {
        let alert = UIAlertController(title: "Alert", message: "Problem Fetching News", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}
