import Foundation
import UIKit


protocol MovieDetailPresenterToViewProtocol: class {
    func showMovies()
    func showError()
}


protocol MovieDetailInteractorToPresenterProtocol: class {
    func movieDetailFetched()
    func movieDetailFetchedFailed()
}


protocol MovieDetailPresenterToInteractorProtocol: class {
    var presenter: MovieDetailInteractorToPresenterProtocol? { get set }
    var movieDetailFetchData: MovieDetailModel? { get }

    func fetchMovies(param: Int)
}

protocol MovieDetailViewToPresenterProtocol: class {
    var view: MovieDetailPresenterToViewProtocol? { get set }
    var interactor: MovieDetailPresenterToInteractorProtocol? { get set }
    var router: MovieDetailPresenterToRouterProtocol? { get set }
    
    func updateDetailView()
    func getMovieDetailData() -> MovieDetailModel?
}

protocol MovieDetailPresenterToRouterProtocol: class {
    static func createModule(param: Int) -> UIViewController
}
