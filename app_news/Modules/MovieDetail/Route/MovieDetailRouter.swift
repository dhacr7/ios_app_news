import Foundation
import UIKit

class MovieDetailRouter: MovieDetailPresenterToRouterProtocol{
  
    class func createModule(param: Int) -> UIViewController {

        let view = MovieDetailViewController()
        let presenter: MovieDetailViewToPresenterProtocol & MovieDetailInteractorToPresenterProtocol = MovieDetailPresenter(param)
        let interactor: MovieDetailPresenterToInteractorProtocol = MovieDetailInteractor()
        let router: MovieDetailPresenterToRouterProtocol = MovieDetailRouter()

        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter

        return view
    }

    static var mainstoryboard: UIStoryboard {
        return UIStoryboard(name:"main",bundle: Bundle.main)
    }

}
