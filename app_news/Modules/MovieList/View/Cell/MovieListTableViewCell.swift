import UIKit

class MovieListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var releaseDate: UILabel!
    @IBOutlet weak var overview: UILabel!
    @IBOutlet weak var movieImageView: UIImageView!
    
    func setCell(title: String, author: String, desc: String, imageUrl: String){
        titleLabel.text = title
        releaseDate.text = author
        overview.text = desc
        let url = URL(string: Constants.imageUrl+imageUrl )
        movieImageView.load(url: url!)
    }

}

extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
