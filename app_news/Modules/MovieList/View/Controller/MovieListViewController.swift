import UIKit
import Alamofire

class MovieListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!

    
    // MARK: - Properties
    var presenter: MovieListViewToPresenterProtocol?

    
    // MARK: - Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
}

// MARK: - Setup View
extension MovieListViewController {
    func setup(){
        setUpTableView()
        setupSearchBar()
        bindPresenter()
    }
    
    func setUpTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        tableView.register(UINib(nibName: "MovieListTableViewCell", bundle: .main), forCellReuseIdentifier: "MovieListTableViewCell")
    }
    
    func setupSearchBar(){
        searchBar.delegate = self
    }
   
    func bindPresenter(){
        guard let presenter = presenter else { return }
        presenter.updateView()
    }
    
}

// MARK: - UITableViewDataSource
extension MovieListViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.getMovieListCount() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MovieListTableViewCell", for: indexPath) as? MovieListTableViewCell
        let row = indexPath.row
        let news = presenter?.getMovies(index: row)
        guard let title = news?.title, let author = news?.release_date, let description = news?.overview, let movieImageView = news?.poster_path else {
            return cell ?? UITableViewCell()
        }
        cell?.setCell(title: title, author: author, desc: description, imageUrl: movieImageView)
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let presenters = presenter else { return }
        let row = indexPath.row
        let moviesData = presenter?.getMovies(index: row)
        presenters.showDetailMovieController(navigationController: navigationController!, param: moviesData?.id ?? 0)
    }
    
}

// MARK: - MovieListPresenterToViewProtocol
extension MovieListViewController: MovieListPresenterToViewProtocol {

    func showMovies() {
        tableView.reloadData()
    }
    
    func showError() {
        let alert = UIAlertController(title: "Alert", message: "Problem Fetching News", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: - UISearchBarDelegate
extension MovieListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let presenter = presenter else { return }
        presenter.updateSearchView(param: searchText)
    }
}
