import Foundation
import UIKit


protocol MovieListPresenterToViewProtocol: class {
    func showMovies()
    func showError()
}


protocol MovieListInteractorToPresenterProtocol: class {
    func movieListFetched()
    func movieListFetchedFailed()
}


protocol MovieListPresenterToInteractorProtocol: class {
    var presenter: MovieListInteractorToPresenterProtocol? { get set }
    var movies: [MovieListModel]? { get }
    
    func fetchMovies()
    func fetchSearchMovies(param: String?)
}

protocol MovieListViewToPresenterProtocol: class {
    var view: MovieListPresenterToViewProtocol? { get set }
    var interactor: MovieListPresenterToInteractorProtocol? { get set }
    var router: MovieListPresenterToRouterProtocol? { get set }
    
    func updateView()
    func updateSearchView(param: String?)
    func getMovieListCount() -> Int?
    func getMovies(index: Int) -> MovieListModel?
    func showDetailMovieController(navigationController:UINavigationController, param: Int)
}

protocol MovieListPresenterToRouterProtocol: class {
    static func createModule() -> UIViewController
    func pushToMovieScreen(navigationConroller:UINavigationController, param: Int)
}
