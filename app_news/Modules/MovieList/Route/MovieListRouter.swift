import Foundation
import UIKit

class MovieListRouter: MovieListPresenterToRouterProtocol{

    class func createModule() -> UIViewController {

        let view = MovieListViewController()
        let presenter: MovieListViewToPresenterProtocol & MovieListInteractorToPresenterProtocol = MovieListPresenter()
        let interactor: MovieListPresenterToInteractorProtocol = MovieListInteractor()
        let router: MovieListPresenterToRouterProtocol = MovieListRouter()

        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter

        return view
    }

    static var mainstoryboard: UIStoryboard {
        return UIStoryboard(name:"main",bundle: Bundle.main)
    }
    
    func pushToMovieScreen(navigationConroller: UINavigationController, param: Int) {
        let movieModule = MovieDetailRouter.createModule(param: param)
        navigationConroller.pushViewController(movieModule,animated: true)
    }

}
