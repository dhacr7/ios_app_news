import Foundation
import UIKit


class MovieListPresenter: MovieListViewToPresenterProtocol {

    var view: MovieListPresenterToViewProtocol?
    var interactor: MovieListPresenterToInteractorProtocol?
    var router: MovieListPresenterToRouterProtocol?

    func updateView() {
        interactor?.fetchMovies()
    }
    
    func updateSearchView(param: String?) {
        if param == nil || param == "" {
            interactor?.fetchMovies()
        }else{
            interactor?.fetchSearchMovies(param: param)
        }
    }

    func getMovieListCount() -> Int? {
        return interactor?.movies?.count
    }

    func getMovies(index: Int) -> MovieListModel? {
        return interactor?.movies?[index]
    }
    
    func showDetailMovieController(navigationController: UINavigationController, param: Int) {
        router?.pushToMovieScreen(navigationConroller:navigationController, param: param)
    }
    
}

extension MovieListPresenter: MovieListInteractorToPresenterProtocol {
    func movieListFetched() {
        view?.showMovies()
    }
    
    func movieListFetchedFailed() {
        view?.showError()
    }
    
}

