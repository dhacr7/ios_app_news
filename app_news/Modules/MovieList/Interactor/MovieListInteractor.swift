import Foundation
import Alamofire
import UIKit

class MovieListInteractor: MovieListPresenterToInteractorProtocol {
    
    weak var presenter: MovieListInteractorToPresenterProtocol?
    var movies: [MovieListModel]?
    
    func fetchMovies() {
        let setUrl = Constants.baseURL+"/top_rated?api_key="+Constants.API_KEY+"&language=en-US&page=1"
        AF.request(setUrl).response { response in
            if (response.response?.statusCode == 200){
                guard let data = response.data else {return}
                do {
                    let decoder = JSONDecoder()
                    let movieResponse = try decoder.decode(MovieResponse.self, from: data)
                    guard let articles = movieResponse.results else {return}
                    self.movies = articles
                    self.presenter?.movieListFetched()
                } catch let error{
                    print(error)
                }
            }else{
                self.presenter?.movieListFetchedFailed()
            }
        }
    }
    
    func fetchSearchMovies(param: String?) {
        let setUrl = Constants.searchURL+"?api_key="+Constants.API_KEY+"&language=en-US&query=\(param ?? "all")&page=1&include_adult=false"
        AF.request(setUrl).response { response in
            if (response.response?.statusCode == 200){
                guard let data = response.data else {return}
                do {
                    let decoder = JSONDecoder()
                    let movieResponse = try decoder.decode(MovieResponse.self, from: data)
                    guard let articles = movieResponse.results else {return}
                    self.movies = articles
                    self.presenter?.movieListFetched()
                } catch let error{
                    print(error)
                }
            }else{
                self.presenter?.movieListFetchedFailed()
            }
        }
    }
    
}

